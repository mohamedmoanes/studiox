﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace spacex
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        string token;


        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

          

            this.Frame.Navigate(typeof(CoworkingSpace),token);

        }



        private void kitchen_btn_Click_1(object sender, RoutedEventArgs e)
        {
            
            this.Frame.Navigate(typeof(kitchenxaml),token);

        }

        private void login_btn_Click(object sender, RoutedEventArgs e)
        {
            loginAsync();
        }
           async Task loginAsync()
        {
            progresRing.IsActive = true;

            var formContent = new FormUrlEncodedContent(new[]
               {
                      new KeyValuePair<string, string>("email", email.Text)  ,
                      new KeyValuePair<string, string>("password", password.Password)
            });

            var myHttpClient = new HttpClient();
            var response = await myHttpClient.PostAsync("https://studiox.000webhostapp.com/api/v1/login", formContent);
            var stringContent = await response.Content.ReadAsStringAsync();
            try
            {
                var model = JsonConvert.DeserializeObject<RootObject>(stringContent);
                if (model.scode.Equals(200))
                {
                    token = model.data.token;
                    SharedData sharedData = new SharedData();
                    sharedData.token = token;
                    kitchen_btn.IsEnabled = true;
                    coworking_btn.IsEnabled = true;
                    progresRing.IsActive = false;

                    var dialog = new MessageDialog("welcome " + model.data.name);
                    await dialog.ShowAsync();
                }
              
            }
            catch
            {
                try
                {
                    var model = JsonConvert.DeserializeObject<EmailerrorObject>(stringContent);
                    progresRing.IsActive = false;

                    var dialog1 = new MessageDialog(model.message.email[0]);
                    await dialog1.ShowAsync();
                }
                catch
                {
                    progresRing.IsActive = false;

                    var dialog = new MessageDialog("invaled password");
                    await dialog.ShowAsync();
                }
               
            }
          
        }
       public string gettoken()
        {
            return token;
        }

        public class Data
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string role { get; set; }
            public object deleted_at { get; set; }
            public string created_at { get; set; }
            public string updated_at { get; set; }
            public string token { get; set; }
        }

        public class RootObject
        {
            public int scode { get; set; }
            public bool success { get; set; }
            public Data data { get; set; }
            public string message { get; set; }
        }

        public class EmailMessage
        {
            public List<string> email { get; set; }
        }

        public class EmailerrorObject
        {
            public int scode { get; set; }
            public bool success { get; set; }
            public EmailMessage message { get; set; }
        }
    }
}
