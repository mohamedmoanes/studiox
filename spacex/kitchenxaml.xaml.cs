﻿using Newtonsoft.Json;
using SDKTemplate;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.PointOfService;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Display;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace spacex
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class kitchenxaml : Page
    {
        List<Datum> userList = new List<Datum>();
        string token;


        ObservableCollection<BarcodeScannerInfo> barcodeScanners = new ObservableCollection<BarcodeScannerInfo>();

        DeviceWatcher watcher;
        bool isSelectionChanging = false;
        string pendingSelectionDeviceId = null;
        bool isStopPending = false;
        static readonly Guid rotationGuid = new Guid("C380465D-2271-428C-9B83-ECEA3B4A85C1");

        DisplayRequest displayRequest = new DisplayRequest();
        MediaCapture mediaCapture;
        BarcodeScanner selectedScanner = null;
        ClaimedBarcodeScanner claimedScanner = null;
        public bool IsScannerClaimed { get; set; } = false;
        public bool IsPreviewing { get; set; } = false;
        public bool ScannerSupportsPreview { get; set; } = false;
        public bool SoftwareTriggerStarted { get; set; } = false;
        public event PropertyChangedEventHandler PropertyChanged;

        public kitchenxaml()
        {
            this.InitializeComponent();

            watcher = DeviceInformation.CreateWatcher(BarcodeScanner.GetDeviceSelector());
            watcher.Added += Watcher_Added;
            watcher.Removed += Watcher_Removed;
            watcher.Updated += Watcher_Updated;
            watcher.Start();

            //gettable();


        }

        private async void Watcher_Added(DeviceWatcher sender, DeviceInformation args)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                barcodeScanners.Add(new BarcodeScannerInfo(args.Name, args.Id));

                // Select the first scanner by default.
                if (barcodeScanners.Count == 1)
                {
                    //ScannerListBox.SelectedIndex = 0;
                }
            });
        }
        private void Watcher_Removed(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            // We don't do anything here, but this event needs to be handled to enable realtime updates.
            // See https://aka.ms/devicewatcher_added.
        }

        private void Watcher_Updated(DeviceWatcher sender, DeviceInformationUpdate args)
        {
            // We don't do anything here, but this event needs to be handled to enable realtime updates.
            //See https://aka.ms/devicewatcher_added.
        }

        protected async override void OnNavigatedFrom(NavigationEventArgs e)
        {
            watcher.Stop();

            if (isSelectionChanging)
            {
                // If selection is changing, then let it know to stop media capture
                // when it's done.
                isStopPending = true;
            }
            else
            {
                // If selection is not changing, then it's safe to stop immediately.
                await CloseScannerResourcesAsync();
            }
        }
        /// <summary>
        /// Starts previewing the selected scanner's video feed and prevents the display from going to sleep.
        /// </summary>
        private async Task StartMediaCaptureAsync(string videoDeviceId)
        {
            mediaCapture = new MediaCapture();

            // Register for a notification when something goes wrong
            mediaCapture.Failed += MediaCapture_Failed;

            var settings = new MediaCaptureInitializationSettings
            {
                VideoDeviceId = videoDeviceId,
                StreamingCaptureMode = StreamingCaptureMode.Video,
            };

            // Initialize MediaCapture
            bool captureInitialized = false;
            try
            {
                await mediaCapture.InitializeAsync(settings);
                captureInitialized = true;
            }
            catch (UnauthorizedAccessException)
            {
                //rootPage.NotifyUser("The app was denied access to the camera", NotifyType.ErrorMessage);
            }
            catch (Exception e)
            {
                //rootPage.NotifyUser("Failed to initialize the camera: " + e.Message, NotifyType.ErrorMessage);
            }

            if (captureInitialized)
            {
                // Prevent the device from sleeping while the preview is running.
                displayRequest.RequestActive();

                //PreviewControl.Source = mediaCapture;
                //await mediaCapture.StartPreviewAsync();
                //await SetPreviewRotationAsync(DisplayInformation.GetForCurrentView().CurrentOrientation);
                //IsPreviewing = true;
                //RaisePropertyChanged(nameof(IsPreviewing));
            }
            else
            {
                mediaCapture.Dispose();
                mediaCapture = null;
            }
        }
        public void RaisePropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void MediaCapture_Failed(MediaCapture sender, MediaCaptureFailedEventArgs errorEventArgs)
        {
            //rootPage.NotifyUser("Media capture failed. Make sure the camera is still connected.", NotifyType.ErrorMessage);
        }
        /// <summary>
        /// Close the scanners and stop the preview.
        /// </summary>
        private async Task CloseScannerResourcesAsync()
        {
            claimedScanner?.Dispose();
            claimedScanner = null;

            selectedScanner?.Dispose();
            selectedScanner = null;

            SoftwareTriggerStarted = false;
            RaisePropertyChanged(nameof(SoftwareTriggerStarted));

            if (IsPreviewing)
            {
                if (mediaCapture != null)
                {
                    await mediaCapture.StopPreviewAsync();
                    mediaCapture.Dispose();
                    mediaCapture = null;
                }

                // Allow the display to go to sleep.
                displayRequest.RequestRelease();

                IsPreviewing = false;
                RaisePropertyChanged(nameof(IsPreviewing));
            }
        }
        private async Task SetPreviewRotationAsync(DisplayOrientations displayOrientation)
        {
            bool isExternalCamera;
            bool isPreviewMirrored;

            // Figure out where the camera is located to account for mirroring and later adjust rotation accordingly.
            DeviceInformation cameraInformation = await DeviceInformation.CreateFromIdAsync(selectedScanner.VideoDeviceId);

            if ((cameraInformation.EnclosureLocation == null) || (cameraInformation.EnclosureLocation.Panel == Windows.Devices.Enumeration.Panel.Unknown))
            {
                isExternalCamera = true;
                isPreviewMirrored = false;
            }
            else
            {
                isExternalCamera = false;
                isPreviewMirrored = (cameraInformation.EnclosureLocation.Panel == Windows.Devices.Enumeration.Panel.Front);
            }

            //PreviewControl.FlowDirection = isPreviewMirrored ? FlowDirection.RightToLeft : FlowDirection.LeftToRight;

            if (!isExternalCamera)
            {
                // Calculate which way and how far to rotate the preview.
                int rotationDegrees = 0;
                switch (displayOrientation)
                {
                    case DisplayOrientations.Portrait:
                        rotationDegrees = 90;
                        break;
                    case DisplayOrientations.LandscapeFlipped:
                        rotationDegrees = 180;
                        break;
                    case DisplayOrientations.PortraitFlipped:
                        rotationDegrees = 270;
                        break;
                    case DisplayOrientations.Landscape:
                    default:
                        rotationDegrees = 0;
                        break;
                }

                // The rotation direction needs to be inverted if the preview is being mirrored.
                if (isPreviewMirrored)
                {
                    rotationDegrees = (360 - rotationDegrees) % 360;
                }

                // Add rotation metadata to the preview stream to make sure the aspect ratio / dimensions match when rendering and getting preview frames.
                var streamProperties = mediaCapture.VideoDeviceController.GetMediaStreamProperties(MediaStreamType.VideoPreview);
                streamProperties.Properties[rotationGuid] = rotationDegrees;
                await mediaCapture.SetEncodingPropertiesAsync(MediaStreamType.VideoPreview, streamProperties, null);
            }
        }
        private async void ScannerSelection_Changed(object sender, SelectionChangedEventArgs args)
        {
            var selectedScannerInfo = barcodeScanners[0];
            var deviceId = selectedScannerInfo.DeviceId;

            if (isSelectionChanging)
            {
                pendingSelectionDeviceId = deviceId;
                return;
            }

            do
            {
                await SelectScannerAsync(deviceId);

                // Stop takes precedence over updating the selection.
                if (isStopPending)
                {
                    await CloseScannerResourcesAsync();
                    break;
                }

                deviceId = pendingSelectionDeviceId;
                pendingSelectionDeviceId = null;
            } while (!String.IsNullOrEmpty(deviceId));
        }

        private async Task SelectScannerAsync(string scannerDeviceId)
        {
            isSelectionChanging = true;

            await CloseScannerResourcesAsync();

            selectedScanner = await BarcodeScanner.FromIdAsync(scannerDeviceId);

            if (selectedScanner != null)
            {
                claimedScanner = await selectedScanner.ClaimScannerAsync();
                if (claimedScanner != null)
                {
                    await claimedScanner.EnableAsync();
                    ScannerSupportsPreview = !String.IsNullOrEmpty(selectedScanner.VideoDeviceId);
                    RaisePropertyChanged(nameof(ScannerSupportsPreview));

                    claimedScanner.DataReceived += ClaimedScanner_DataReceived;

                    if (ScannerSupportsPreview)
                    {
                        await StartMediaCaptureAsync(selectedScanner.VideoDeviceId);
                    }
                }
                else
                {
                    //rootPage.NotifyUser("Failed to claim the selected barcode scanner", NotifyType.ErrorMessage);
                }

            }
            else
            {
                //rootPage.NotifyUser("Failed to create a barcode scanner object", NotifyType.ErrorMessage);
            }

            IsScannerClaimed = claimedScanner != null;
            RaisePropertyChanged(nameof(IsScannerClaimed));

            isSelectionChanging = false;
        }

        private async void ClaimedScanner_DataReceived(ClaimedBarcodeScanner sender, BarcodeScannerDataReceivedEventArgs args)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                claimedScanner?.HideVideoPreview();
                string id = DataHelpers.GetDataLabelString(args.Report.ScanDataLabel, args.Report.ScanDataType);


                List<int> ids = new List<int>();

                for (int i = 0; i < userList.Count; i++)
                {
                    ids.Add(userList[i].id);

                }
                addproduct(ids, id);

                //if (function.Equals("start"))
                //{
                //    insertstartAsync(id);
                //}
                //else
                //{
                //    updateendAsync(id);
                //}

                //var dialog = new MessageDialog(

                //   DataHelpers.GetDataLabelString(args.Report.ScanDataLabel, args.Report.ScanDataType)
                // );
                //await dialog.ShowAsync();

            });
        }

        async void initAsync()
        {
            string selector = BarcodeScanner.GetDeviceSelector();



            DeviceInformationCollection deviceCollection = await DeviceInformation.FindAllAsync(selector);

        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string text = e.Parameter as string;
            if (text != null)
            {
                token = text;
                gettable();

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
                  addproductAsync();
        }

        async Task addproductAsync()
        {
       
            
               
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                      new KeyValuePair<string, string>("name", name_input.Text),
                      new KeyValuePair<string, string>("price", price_input.Text)   
                    });

                    var myHttpClient = new HttpClient();
            myHttpClient.DefaultRequestHeaders.Authorization= new AuthenticationHeaderValue("Bearer",token);
            var response = await myHttpClient.PostAsync("https://studiox.000webhostapp.com/api/v1/admin/product/create", formContent);
            var stringContent = await response.Content.ReadAsStringAsync();

            var model = JsonConvert.DeserializeObject<respo>(stringContent);
            if (model.scode.Equals("200"))
            {
                var dialog = new MessageDialog("Done");
                await dialog.ShowAsync();
            }

        }

        void gettable()
        {
            using (var httpClient = new HttpClient())
            {
                //httpClient.DefaultRequestHeaders;
                try
                {
                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjYyY2IyODE3YWFkNmFmOWNiNDI4OWNhMDc4YWM3ZDc1MmZiNDlhYTBhODFjYWQ2ZjQ1ZjczZjMxNDExZjUwOWU0ZTFjM2JiNTI3OGE5ZTY2In0.eyJhdWQiOiIzIiwianRpIjoiNjJjYjI4MTdhYWQ2YWY5Y2I0Mjg5Y2EwNzhhYzdkNzUyZmI0OWFhMGE4MWNhZDZmNDVmNzNmMzE0MTFmNTA5ZTRlMWMzYmI1Mjc4YTllNjYiLCJpYXQiOjE1Mzk3MzIwNjgsIm5iZiI6MTUzOTczMjA2OCwiZXhwIjoxNTcxMjY4MDY4LCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.Re82V8-mkKnbYMCSlEkTeDnvbF2JU38b5fACeqkzfWDHa81gwVDClzrDNCXqp9rCoRF_VJjyf_w9MuIW3UqhnHZjuwm5ECJRkbnFNcQCPoMynDN0gRyEFUwFBwpVNsmS-fBlDxL8d41aQbA7-pk1earAzhL3dJDH-XHUIi1sTbH3nAdw-00ngCt_3CxdIjwsyXL3lTfrlHxLGS-Nzld3JNMhjY5kKMy6j7FIBwYf1VNsOVYsAvo-f_4w4kaEq7e4dxLmdb9Z8gtRSqzlToOGvVmIwlQJsL0_89qmNItv2u3wz55mD2dkfEp7wiPr_g_AjdLCERJSB4x3pQ2g0aJcIkeF9UCsQjcYYjtElM3AFsEqey1FlDeWOHScpipdZEwMnaeMNV_ifPEr9LBwezF66sP9A7Ml18P8EwZR2F8hcOO22Fp_BUExmUlNV8Fxafv-2_jt_8KCYbMFCeDW17QddzbogJoa3iZXonzVcN6fb3z-zAASiq0cMWsuNkzde-uCa1xgh9OOVW3Fg-b6OJ7B0VjDO_8DbQj7Ccq8SbgwPVbfKR-7H7-GoZ506AFY5PeR6wQMmXO_Le1vGsbf2JKc5zF3419y8d6p5JFuinOA68cJ9bnYxDoBwVFbfpYesqk3wO6jkmvxWWqlf5VvhtqnnN3k-pGTOEBqSRkj5BPnGGU");
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    var response = httpClient.GetStringAsync(new Uri("https://studiox.000webhostapp.com/api/v1/admin/product/get-all")).Result;
                    var model = JsonConvert.DeserializeObject<RootObject>(response);

                    productstable.ItemsSource = model.data.data;
                    
                }
                catch
                {

                }
            }
        }

 


public class Datum
{
    public int id { get; set; }
    public string name { get; set; }
    public string price { get; set; }
    //public object deleted_at { get; set; }
    //public string created_at { get; set; }
    //public string updated_at { get; set; }
}

public class Data
{
    public int current_page { get; set; }
    public List<Datum> data { get; set; }
    public string first_page_url { get; set; }
    public int from { get; set; }
    public int last_page { get; set; }
    public string last_page_url { get; set; }
    public object next_page_url { get; set; }
    public string path { get; set; }
    public int per_page { get; set; }
    public object prev_page_url { get; set; }
    public int to { get; set; }
    public int total { get; set; }
}

public class RootObject
{
    public int scode { get; set; }
    public bool success { get; set; }
    public Data data { get; set; }
    public string message { get; set; }
}
public class respo
        {
            public string scode { get; set; }    
            public Boolean success { get; set; }    
            
        }

        private void Add_to_list_Click(object sender, RoutedEventArgs e)
        {
            userlist.ItemsSource = null;
            
            userList.Add((Datum)productstable.SelectedItem);
            userlist.ItemsSource = userList;
                   


        }

        private void remove_from_list_Click(object sender, RoutedEventArgs e)
        {

            userList.RemoveAt(userlist.SelectedIndex);
            userlist.ItemsSource = null;

            userlist.ItemsSource = userList;
        }

        private async void confirm_ClickAsync(object sender, RoutedEventArgs e)
        {
            var selectedScannerInfo = barcodeScanners[0];
            var deviceId = selectedScannerInfo.DeviceId;

            if (isSelectionChanging)
            {
                pendingSelectionDeviceId = deviceId;
                return;
            }

            do
            {

                await SelectScannerAsync(deviceId);

                // Stop takes precedence over updating the selection.
                if (isStopPending)
                {
                    await CloseScannerResourcesAsync();
                    break;
                }

                deviceId = pendingSelectionDeviceId;
                pendingSelectionDeviceId = null;
            } while (!String.IsNullOrEmpty(deviceId));

            if (claimedScanner != null)
            {
                await claimedScanner.StartSoftwareTriggerAsync();

                SoftwareTriggerStarted = true;
                RaisePropertyChanged(nameof(SoftwareTriggerStarted));
            }
            await claimedScanner?.ShowVideoPreviewAsync();

            //getcamAsync();
        }

        async Task getcamAsync()
        {
            CameraCaptureUI captureUI = new CameraCaptureUI();
            captureUI.PhotoSettings.Format = CameraCaptureUIPhotoFormat.Jpeg;
            captureUI.PhotoSettings.CroppedSizeInPixels = new Size(200, 200);

            StorageFile photo = await captureUI.CaptureFileAsync(CameraCaptureUIMode.Photo);

           

                DecodeQRCode(photo);
           
            if (photo == null)
            {
                // User cancelled photo capture
                return;
            }


        }
        public async void DecodeQRCode(StorageFile file)
        {
            try
            {
                // load a jpeg, be sure to have the Pictures Library capability in your manifest
                var data = await FileIO.ReadBufferAsync(file);

                // create a stream from the file
                var ms = new InMemoryRandomAccessStream();
                var dw = new Windows.Storage.Streams.DataWriter(ms);
                dw.WriteBuffer(data);
                await dw.StoreAsync();
                ms.Seek(0);

                // find out how big the image is, don't need this if you already know
                var bm = new BitmapImage();
                await bm.SetSourceAsync(ms);

                // create a writable bitmap of the right size
                var wb = new WriteableBitmap(bm.PixelWidth, bm.PixelHeight);
                ms.Seek(0);

                // load the writable bitpamp from the stream
                await wb.SetSourceAsync(ms);

                var lsource = new BitmapLuminanceSource(wb);

                var binarizer = new HybridBinarizer(lsource);
                var bbmp = new BinaryBitmap(binarizer);

                var c = new QRCodeReader();
                Result res = c.decode(bbmp);

                if (res == null)
                {
                    var dialog = new MessageDialog("error bad photo");
                    await dialog.ShowAsync();
                    return;
                }
                else
                {
                    List<int> ids = new List<int>();

                    for (int i = 0; i < userList.Count; i++)
                    {
                        ids.Add(userList[i].id);

                    }

                    //insertstartAsync(res.Text);
                    addproduct(ids, res.Text);
                   
                 
              
                }
            }
            catch
            {

            }

        }

        async Task addproduct(List<int>ids,string userid)
        {
            string activtyid;

            var formContent = new FormUrlEncodedContent(new[]
                  {
                      new KeyValuePair<string, string>("user_id", userid)
                     
                    });

            var myHttpClient = new HttpClient();
            myHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await myHttpClient.PostAsync("https://studiox.000webhostapp.com/api/v1/user/get-last-activity", formContent);
            var stringContent = await response.Content.ReadAsStringAsync();

            var activtymodel = JsonConvert.DeserializeObject<ActivtyObject>(stringContent);
            if (activtymodel.data.end_time == null)
            {
                activtyid = activtymodel.data.id.ToString();

                for (int i = 0; i < ids.Count; i++)
                {
                    var formdata = new FormUrlEncodedContent(new[]
              {
                      new KeyValuePair<string, string>("product_id", ids[i].ToString())    ,
                      new KeyValuePair<string, string>("activity_id", activtyid)   ,
                      new KeyValuePair<string, string>("qty", "1")

                    });

                    var HttpClient = new HttpClient();
                    HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var addresponse = await HttpClient.PostAsync("https://studiox.000webhostapp.com/api/v1/user/add-product", formdata);
                    var content = await response.Content.ReadAsStringAsync();

                    var model = JsonConvert.DeserializeObject<respo>(content);
                    if(model.scode!="200" && model.success != true)
                    {
                        var dialog = new MessageDialog("number "+i+" not added");
                        await dialog.ShowAsync();
                    }
                    else
                    {
                        var dialog = new MessageDialog("done");
                        await dialog.ShowAsync();
                    }
                }



            }
            else{
                var dialog = new MessageDialog("this user is not in the coworking space");
                await dialog.ShowAsync();
            }
        }
        


public class Option
{
    public string name { get; set; }
    public string price { get; set; }
}

public class ActivtyData
{
    public int id { get; set; }
    public string user_id { get; set; }
    public string start_time { get; set; }
    public object end_time { get; set; }
    public string day { get; set; }
    public string area_id { get; set; }
    public List<Option> options { get; set; }
    public object deleted_at { get; set; }
    public string created_at { get; set; }
    public string updated_at { get; set; }
}

public class ActivtyObject
{
    public int scode { get; set; }
    public bool success { get; set; }
    public ActivtyData data { get; set; }
    public string message { get; set; }
}

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            On_BackRequested();
        }
        private bool On_BackRequested()
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
                return true;
            }
            return false;
        }

        private void BackInvoked(KeyboardAccelerator sender, KeyboardAcceleratorInvokedEventArgs args)
        {
            On_BackRequested();
            args.Handled = true;
        }
    }


}
